# quila version
VERSION = 0.0

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man
EXAMPLEPREFIX = $(PREFIX)/share/examples
QUILARC_EXAMPLE_SNIPPET=Example configuration file for quila.
QUILARC_ETC_SNIPPET=Default configuration file for quila. Per-user configuration should be done in \$$XDG_CONFIG_HOME\/quila\/quilarc.
