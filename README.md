quila
-----
quila stands for "quick latex". The purpose of quila is to make latex documents very easy to compile. If a set of .tex files are arranged in the specification that quila requires, quila can easily compile different orientations of said files.

install
-------
Currently, quila must be installed from source.
Clone the project and cd into it.

Run `make`

Because quila is a shell script, you are free to simply move it where ever you want. To automate this, run `make install` with root priviledges. If you want to move it to a custom location, you may customize config.mk and the Makefile. Otherwise, do be sure to manually move the quilarc-etc file to /etc/quilarc. In either case, edit the latex_command option to be a valid latex engine installed on your system. See the man page `quilarc(5)` for more details.

quick start
-----------
To use quila, create and cd into a new directory.
Run `quila status`
You should see the following.

    This is not a quila project. Initialize this folder with quila init.
quila understands when directories are not made to be used with it, so it won't start modifying their files and making you lose important work. Run `quila init`. Several files and directories will populate the current directory. Run `quila status` again. You should now see the following.

    Everything is being tracked.
Edit the newly created base/base.tex file and add whatever you want.

    $ cat base/base.tex
    Include whatever you want in this file.
    Of course, you can include \LaTeX{} commands in here.
    \begin{center}
        See?
    \end{center}
Now try `quila compile`. If you view the .tex file before compiling, you can see that a very basic LaTeX document has been generated. It becomes clear now that the base.tex file you edited in included in this file and thus in the final document.

Assuming there were no errors, a document will appear in the output directory. For most LaTeX engines, this document will be a PDF. Opening this PDF in a PDF viewer, you will see the text you added to the base.tex file, typeset with LaTeX.

Now cd into the base directory from before and create a new directory. Run `quila status` afterwards.

    $ quila status
    Untracked files:
      (use "quila add file ..." to add them to the database)
            base/new-dir-i-just-made
quila will not do anything with new files if they aren't added to the database, so add your new directory. quila actually will not do anything either way if the directory is in the database but is empty. Additionally, the directory must contain either a file with the same name as it and .tex appended or another directory.

    $ cat new-dir-i-just-made/new-dir-i-just-made.tex
    This file will be interpreted as a \textbf{section} by \LaTeX.
Again, compile the LaTeX document but edit the .tex file before compiling. Mine looks like this.

    \documentclass{article}

    \begin{document}
    \input{../base/base}
    \section{new-dir-i-just-made}
    \input{../base/new-dir-i-just-made/new-dir-i-just-made}

    \end{document}
You can see that directories inside the base directory are interpreted as sections. This is recursive, so directories inside section directories are interpreted as subsections. Be warned that quila will not warn you when you have too many subdirectories. It will happily place `\subsubsubsubsubsection{...}` into the output .tex file which will fail to compile. As of right now, the only solution is to not have more than three sublevels within the base directory. The third level directories will all be `\subsubsection`s.

As you can see, you don't need to type any LaTeX code to compile nice LaTeX documents with good typesetting.
