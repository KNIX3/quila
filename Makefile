# dmenu - dynamic menu
# See LICENSE file for copyright and license details.

include config.mk

all: quilarc-etc quilarc-example 

quilarc-example:
	sed "s/SNIPPET/${QUILARC_EXAMPLE_SNIPPET}/g" quilarc > $@

quilarc-etc:
	sed "s/SNIPPET/${QUILARC_ETC_SNIPPET}/g" quilarc > $@

clean:
	rm -f quilarc-example quilarc-etc

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f quila $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/quila
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f quila.1 $(DESTDIR)$(MANPREFIX)/man1/quila.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/quila.1
	mkdir -p $(DESTDIR)$(MANPREFIX)/man5
	cp -f quilarc.5 $(DESTDIR)$(MANPREFIX)/man5/quilarc.5
	chmod 644 $(DESTDIR)$(MANPREFIX)/man5/quilarc.5
	mkdir -p $(DESTDIR)$(EXAMPLEPREFIX)/quila
	cp -f quilarc-example $(DESTDIR)$(EXAMPLEPREFIX)/quila/quilarc
	cp -f quilarc-etc /etc/quilarc

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/quila\
		$(DESTDIR)$(MANPREFIX)/man1/quila.1\
		$(DESTDIR)$(MANPREFIX)/man5/quilarc.5\
		/etc/quilarc
	rm -rf $(DESTDIR)$(EXAMPLEPREFIX)/quila

.PHONY: all clean install uninstall
